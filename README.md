## TruthWeightTools

This tool provides easy access to the theoretical uncertainties relevant to Higgs MC samples (HiggsWeightTool would have been a more approriate name).
Earlier version of this tool exsit in [SVN: atlasoff/PhysicsAnalysis/TruthWeightTools](https://svnweb.cern.ch/trac/atlasoff/browser/PhysicsAnalysis/TruthWeightTools/trunk) and also in the HGam git. 

The tool can be seen as a wrapper around the PMGTruthWeightTool that is in the [PMGTools package](https://gitlab.cern.ch/atlas/athena/tree/21.2/PhysicsAnalysis/AnalysisCommon/PMGTools).
It has an internal such object, and accesses all weights and MetaData though this tool.

The most significant functionality of this tool is to provide theoretical uncertaitnies for the ggF process, derived for the Powheg ggF NNLOPS generator.
They have been derived to cover differences between the Powheg NNLOPS ggF prediction and state of the art predictions.

For usage, see this [old example](https://svnweb.cern.ch/trac/atlasoff/browser/PhysicsAnalysis/TruthWeightTools/trunk/util/HiggsWeightTest.cxx), that also produces several of the resutls shown in the talks below.

NOTE: The "Force" methods shoudl not be used anymore. They were needed as a temporary fix for early data (analyses in 2017) when the MetaData was not yet properly stored in derviations (instead of using the meta data, you force to tool to assume weights stored in a certain order).
"Modern" samples should not need any such (ugly) hacks to work. It's best and safer to use the default AUTO mode.

Several talks has been given during the development of this tool.
1. https://indico.cern.ch/event/618048/ In this meeting (first and last talks) two different ggF uncertainty schemes (WG1 and STXS) are discussed and the final recommendation, called the "2017 uncertainty scheme" is the result of combining the "best parts" of the two.
2. https://indico.cern.ch/event/617739/ This shows example usage for the old tool in SVN, but more importantly, many uncertainty results.
3. https://indico.cern.ch/event/616388/ Initial talk. Less complete than the above one.
