/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

/// Example standalone executable using TEvent (from POOL or xAODRootAccess) to read an xAOD
/// Tests the HiggsWeightTool, showing example usage

/// @author: James Robinson

// STL include(s):
#include <chrono>

// EDM include(s):
#include "AsgTools/MessageCheck.h"
#include "AsgTools/AnaToolHandle.h"

// Project dependent include(s)
#ifdef XAOD_STANDALONE
  #include "xAODRootAccess/Init.h"
  #include "xAODRootAccess/TEvent.h"
#else
  #include "POOLRootAccess/TEvent.h"
#endif

// Local include(s):
#include "TruthWeightTools/HiggsWeightTool.h"

// ROOT include(s):
#include "TFile.h"
#include "TLorentzVector.h"
#include "TRandom.h"


using namespace asg::msgUserCode;  // messaging

int main(int argc, char *argv[])
{
  ANA_CHECK_SET_TYPE(int);  // makes ANA_CHECK return ints if exiting function

  // Initialise the application:
#ifdef XAOD_STANDALONE
  StatusCode::enableFailure();
  ANA_CHECK(xAOD::Init());
#else
  IAppMgrUI *app = POOL::Init();
#endif

  // Use a default MC file for testing if none is provided
  // TString fileName = "$ASG_TEST_FILE_MC";
  TString fileName = "/eos/user/j/jrobinso/DAOD/mc16_13TeV.343981.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_gamgam.deriv.DAOD_HIGG1D1.e5607_e5984_s3126_r10201_r10210_p3418_tid13073168_00/DAOD_HIGG1D1.13073168._000005.pool.root.1";

  if (argc < 2) {
    ANA_MSG_WARNING("No file name received, using default file");
  } else {
    fileName = argv[1]; // use the user provided file
  }

  // Initialise TEvent reading
#ifdef XAOD_STANDALONE
  std::unique_ptr<TFile> ptrFile(TFile::Open(fileName, "READ"));
  xAOD::TEvent event(xAOD::TEvent::kClassAccess);
  ANA_CHECK(event.readFrom(ptrFile.get()));
#else
  POOL::TEvent event(POOL::TEvent::kClassAccess);
  event.readFrom(fileName);
#endif
  ANA_MSG_INFO("Opened file: " << fileName);

  // Create the truth weight tool:
  ANA_MSG_INFO("Creating HiggsWeightTool...");
#ifdef XAOD_STANDALONE
  asg::AnaToolHandle<TruthWeightTools::IHiggsWeightTool> weightTool;
  ASG_SET_ANA_TOOL_TYPE(weightTool, TruthWeightTools::HiggsWeightTool);
  weightTool.setName("HiggsWeightTool");
  ANA_CHECK(weightTool.setProperty("RequireFinite", true));
  ANA_CHECK(weightTool.initialize());
#else
  asg::AnaToolHandle<TruthWeightTools::IHiggsWeightTool> weightTool("TruthWeightTools::HiggsWeightTool/HiggsWeightTool");
  ANA_CHECK(weightTool.setProperty("RequireFinite", true));
  ANA_CHECK(weightTool.retrieve());
#endif

  // Loop over a few events:
  ANA_MSG_INFO("Preparing to loop over events...");
  const Long64_t nEntries = 5;

  for (Long64_t entry = 0; entry < nEntries; ++entry) {
    if (event.getEntry(entry) < 0) { ANA_MSG_ERROR("Failed to read event " << entry); continue; }

    // Build a dummy Higgs
    TLorentzVector h;
    h.SetPtEtaPhiM(gRandom->Gaus(0, 40000.), 0, 0, 125000.);
    int HTXS_Njets30(gRandom->Poisson(0.9));

    // Access all Higgs weights
    TruthWeightTools::HiggsWeights hw = weightTool->getHiggsWeights(HTXS_Njets30, h.Pt(), 0);

    // Print output
    hw.print();
  }

  // Print final statistics
  weightTool->printSummary();

  // trigger finalization of all services and tools created by the Gaudi Application
#ifndef XAOD_STANDALONE
  app->finalize();
#endif

  return 0;
}
