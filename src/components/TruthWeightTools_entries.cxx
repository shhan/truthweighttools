#include "GaudiKernel/DeclareFactoryEntries.h"
#include "TruthWeightTools/HiggsWeightTool.h"

using namespace TruthWeightTools;

DECLARE_TOOL_FACTORY( HiggsWeightTool )

DECLARE_FACTORY_ENTRIES( TruthWeightTools ) {

  DECLARE_TOOL( HiggsWeightTool )

}
